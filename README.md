# README #

### What is this repository for? ###

* Create a static library of useful C programming functions.
    
* Version

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner is Olha Kosiakova
* Contact email: okosiako@student.unit.ua

LIST OF FUNCTIONS:
=========================
**Recoded functions from _libc_:**

+ Functions for operating with _void *_ data:

    1. memset
    1. bzero
    1. memcpy
    1. memccpy
    1. memmove
    1. memchr
    1. memcmp

+ Functions for operating with _char *_ data:

    1. strlen
    1. strdup
    1. strcpy
    1. strncpy
    1. strcat
    1. strncat
    1. strlcat
    1. strchr
    1. strrchr
    1. strstr
    1. strnstr
    1. strcmp
    1. strncmp
    1. atoi
    1. isalpha
    1. isdigit
    1. isalnum
    1. isascii
    1. isprint
    1. toupper
    1. tolower

**Functions that are either not included in the libc, or included in a different form (with descriptions):**

+ Functions that works with _memory_:

    1. ft_memalloc (allocates and returns a "fresh" memory area. The memory allocated is initialized to 0. If the alloca- tion fails, the function returns NULL)
    1. ft_memdel (takes as a parameter the address of a memory area that needs to be freed, then puts the pointer to NULL)
    1. ft_strnew (Allocates and returns a “fresh” string end- ing with ’\0’. Each character of the string is initialized at ’\0’. If the allocation fails the function returns NULL)
    1. ft_strdel (Takes as a parameter the address of a string that need to be freed, then sets its pointer to NULL.)
    1. ft_strclr (Sets every character of the string to the value ’\0’)

+ Functions for operating with _char *_ data:

    1. ft_striter (Applies the function f to each character of the string passed as argument. Each character is passed by address to f to be modified if necessary)
    1. ft_striteri (Applies the function f to each character of the string passed as argument, and passing its index as first argument. Each character is passed by address to f to be modified if necessary)
    1. ft_strmap (Applies the function f to each character of the string given as argument to create a “fresh” new string resulting from the successive applications of f)
    1. ft_strmapi (Applies the function f to each character of the string passed as argument by giving its index as first argument to create a “fresh” new string resulting from the successive applications of f)
    1. ft_strequ (Lexicographical comparison between s1 and s2. If the 2 strings are identical the function returns 1, or 0 otherwise)
    1. ft_strnequ (Lexicographical comparison between s1 and s2 up to n characters or until a ’\0’ is reached. If the 2 strings are identical, the function returns 1, or 0 otherwise)
    1. ft_strsub (Allocates and returns a “fresh” substring from the string given as argument. The substring begins at index start
and is of size len. If start and len aren’t refering to a valid substring, the behavior is undefined.  If the allocation fails, the function returns NULL)
    1. ft_strjoin (Allocates and returns a “fresh” string ending with ’\0’, result of the concatenation of s1 and s2. If the allocation fails the function returns NULL)
    1. ft_strtrim (Allocates and returns a copy of the string given as argument without whitespaces at the beginning or at the end of the string. Will be considered as whitespaces the following characters ’ ’, ’\n’ and ’\t’. If s has no whitespaces at the beginning or at the end, the function returns a copy of s. If the allocation fails the function returns NULL)
    1. ft_strsplit (Allocates and returns an array of “fresh” strings (all ending with
’\0’, including the array itself) obtained by spliting s using the character c as a delimiter. If the allocation fails the function returns NULL)
    1. ft_itoa (Allocate and returns a “fresh” string ending with ’\0’ representing the integer n given as argument. Negative numbers must be supported. If the allocation fails, the function returns NULL)
    1. ft_putchar (Outputs the character c to the standard output)
    1. ft_putstr (Outputs the string s to the standard output)
    1. ft_putendl (Outputs the string s to the standard output followed by a ’\n’)
    1. ft_putnbr (Outputs the integer n to the standard output)
    1. ft_putchar_fd (Outputs the char c to the file descriptor fd)
1. ft_putchar (Outputs the character c to the standard output)
    1. ft_putstr_fd (Outputs the string s to the file descriptor fd)
    1. ft_putendl_fd (Outputs the string s to the file descriptor fd followed by a ’\n’)
    1. ft_putnbr_fd (Outputs the integer n to the file descriptor fd)