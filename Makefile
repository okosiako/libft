NAME = libft.a
CC = gcc
CFLAGS = -Wall -Wextra -Werror -O3 -g

LIBFT_HEADERS_FLD = .
LIBFT_OBJECTS_FLD = obj_libft
LIBFT_SRC_FLD = src_libft

LIBFT_HEADERS = libft.h
LIBFT_OBJECTS =\
	$(LIBFT_OBJECTS_FLD)/ft_atoi.o\
	$(LIBFT_OBJECTS_FLD)/ft_bzero.o\
	$(LIBFT_OBJECTS_FLD)/ft_isalnum.o\
	$(LIBFT_OBJECTS_FLD)/ft_isalpha.o\
	$(LIBFT_OBJECTS_FLD)/ft_isascii.o\
	$(LIBFT_OBJECTS_FLD)/ft_isdigit.o\
	$(LIBFT_OBJECTS_FLD)/ft_isprint.o\
	$(LIBFT_OBJECTS_FLD)/ft_itoa.o\
	$(LIBFT_OBJECTS_FLD)/ft_itoa_base.o\
	$(LIBFT_OBJECTS_FLD)/ft_atoi_base.o\
	$(LIBFT_OBJECTS_FLD)/ft_lstadd.o\
	$(LIBFT_OBJECTS_FLD)/ft_lstdel.o\
	$(LIBFT_OBJECTS_FLD)/ft_lstdelone.o\
	$(LIBFT_OBJECTS_FLD)/ft_lstiter.o\
	$(LIBFT_OBJECTS_FLD)/ft_lstlast.o\
	$(LIBFT_OBJECTS_FLD)/ft_lstlen.o\
	$(LIBFT_OBJECTS_FLD)/ft_lstmap.o\
	$(LIBFT_OBJECTS_FLD)/ft_lstnadd.o\
	$(LIBFT_OBJECTS_FLD)/ft_lstnew.o\
	$(LIBFT_OBJECTS_FLD)/ft_memalloc.o\
	$(LIBFT_OBJECTS_FLD)/ft_realloc.o\
	$(LIBFT_OBJECTS_FLD)/ft_memccpy.o\
	$(LIBFT_OBJECTS_FLD)/ft_memchr.o\
	$(LIBFT_OBJECTS_FLD)/ft_memcmp.o\
	$(LIBFT_OBJECTS_FLD)/ft_memcpy.o\
	$(LIBFT_OBJECTS_FLD)/ft_memdel.o\
	$(LIBFT_OBJECTS_FLD)/ft_memmove.o\
	$(LIBFT_OBJECTS_FLD)/ft_memset.o\
	$(LIBFT_OBJECTS_FLD)/ft_putchar.o\
	$(LIBFT_OBJECTS_FLD)/ft_putchar_fd.o\
	$(LIBFT_OBJECTS_FLD)/ft_putendl.o\
	$(LIBFT_OBJECTS_FLD)/ft_putendl_fd.o\
	$(LIBFT_OBJECTS_FLD)/ft_putnbr.o\
	$(LIBFT_OBJECTS_FLD)/ft_putnbr_fd.o\
	$(LIBFT_OBJECTS_FLD)/ft_putnbr_l.o\
	$(LIBFT_OBJECTS_FLD)/ft_putstr.o\
	$(LIBFT_OBJECTS_FLD)/ft_putstr_fd.o\
	$(LIBFT_OBJECTS_FLD)/ft_strcat.o\
	$(LIBFT_OBJECTS_FLD)/ft_strchr.o\
	$(LIBFT_OBJECTS_FLD)/ft_strclr.o\
	$(LIBFT_OBJECTS_FLD)/ft_strcmp.o\
	$(LIBFT_OBJECTS_FLD)/ft_strcpy.o\
	$(LIBFT_OBJECTS_FLD)/ft_strdel.o\
	$(LIBFT_OBJECTS_FLD)/ft_strdup.o\
	$(LIBFT_OBJECTS_FLD)/ft_strequ.o\
	$(LIBFT_OBJECTS_FLD)/ft_striter.o\
	$(LIBFT_OBJECTS_FLD)/ft_striteri.o\
	$(LIBFT_OBJECTS_FLD)/ft_strjoin.o\
	$(LIBFT_OBJECTS_FLD)/ft_strjoincanc.o\
	$(LIBFT_OBJECTS_FLD)/ft_strlcat.o\
	$(LIBFT_OBJECTS_FLD)/ft_strlen.o\
	$(LIBFT_OBJECTS_FLD)/ft_strmap.o\
	$(LIBFT_OBJECTS_FLD)/ft_strmapi.o\
	$(LIBFT_OBJECTS_FLD)/ft_strncat.o\
	$(LIBFT_OBJECTS_FLD)/ft_strncmp.o\
	$(LIBFT_OBJECTS_FLD)/ft_strncpy.o\
	$(LIBFT_OBJECTS_FLD)/ft_strnequ.o\
	$(LIBFT_OBJECTS_FLD)/ft_strnew.o\
	$(LIBFT_OBJECTS_FLD)/ft_strnstr.o\
	$(LIBFT_OBJECTS_FLD)/ft_strrchr.o\
	$(LIBFT_OBJECTS_FLD)/ft_strsplit.o\
	$(LIBFT_OBJECTS_FLD)/ft_strstr.o\
	$(LIBFT_OBJECTS_FLD)/ft_strsub.o\
	$(LIBFT_OBJECTS_FLD)/ft_strtrim.o\
	$(LIBFT_OBJECTS_FLD)/ft_tolower.o\
	$(LIBFT_OBJECTS_FLD)/ft_toupper.o\
	$(LIBFT_OBJECTS_FLD)/ft_wputchar.o


.IGNORE : clean fclean

.PHONY : all clean fclean create_folder_for_objects build_msg_start build_msg_end $(NAME)

all : build_msg_start $(NAME) build_msg_end

$(NAME) : create_folder_for_objects begin_compiling_msg $(LIBFT_OBJECTS) end_compiling_msg $(LIBFT_HEADERS)
	@ar rc $(NAME) $(LIBFT_OBJECTS) $(OBJECTS_2)
	@ranlib $(NAME)

$(LIBFT_OBJECTS_FLD)/%.o: %.c
	@$(CC) -I $(LIBFT_HEADERS_FLD) $(CFLAGS) -c $< -o $@

vpath %.c $(LIBFT_SRC_FLD)

create_folder_for_objects :
	@mkdir -p $(LIBFT_OBJECTS_FLD)

begin_compiling_msg :
	@printf "\033[34m Begin compiling ...\n\033[0m"

end_compiling_msg :
	@printf "\033[32m Done ...\n\033[0m"

build_msg_start :
	@printf "\033[1;34m\n Start building \033[31m$(NAME)\033[1;34m\033[0m\n"

build_msg_end :
	@printf "\033[32m $(NAME)	created\n\033[0m####################\n\n"

clean :
	rm -rf $(LIBFT_OBJECTS_FLD)

fclean : clean
	rm -f $(NAME)

re : fclean all
