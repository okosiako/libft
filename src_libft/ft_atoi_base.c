/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/07 13:45:49 by okosiako          #+#    #+#             */
/*   Updated: 2017/06/28 10:39:09 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static long long	sqr(long long num, unsigned int level)
{
	long long	res;

	res = num;
	while (level > 1)
	{
		res *= num;
		level--;
	}
	return (level ? res : 1);
}

long long			ft_atoi_base(const char *num, int base)
{
	long long	res;
	int			size;
	char		*base_base;
	size_t		index;

	size = ft_strlen(num) - 1;
	res = 0;
	index = 0;
	base_base = "0123456789ABCDEF";
	while (size >= 0)
	{
		res += sqr(base, size) * (ft_strchr(base_base, num[index]) - base_base);
		size--;
		index++;
	}
	return (res);
}
