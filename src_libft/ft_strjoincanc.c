/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoincanc.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/08 16:42:49 by okosiako          #+#    #+#             */
/*   Updated: 2017/06/08 17:00:16 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoincanc(char **s1, char const *s2)
{
	char *res;

	res = NULL;
	if (*s1 && s2)
	{
		if ((res = (char *)ft_memalloc(sizeof(char) *
						(ft_strlen(*s1) + ft_strlen(s2) + 1))))
		{
			ft_strcat(res, *s1);
			ft_strcat(res, s2);
		}
		ft_strdel(s1);
	}
	return (res);
}
