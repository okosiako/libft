/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 18:04:51 by okosiako          #+#    #+#             */
/*   Updated: 2017/06/06 18:38:10 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_realloc(void *ptr, size_t size)
{
	void *temp;

	if (!(temp = ft_memalloc(size)))
		return (NULL);
	if (!(temp = ft_memccpy(temp, ptr, '\0', size)))
		ft_putstr("Error occured!\n");
	ft_memdel(&ptr);
	return (temp);
}
