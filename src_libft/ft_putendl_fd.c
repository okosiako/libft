/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/03 12:33:01 by okosiako          #+#    #+#             */
/*   Updated: 2016/12/03 12:35:25 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

ssize_t	ft_putendl_fd(char const *s, int fd)
{
	if (s)
	{
		return (ft_putstr_fd(s, fd));
	}
	return (ft_putchar_fd('\n', fd));
}
